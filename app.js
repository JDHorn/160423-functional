var R = require('ramda');
var beerData = require('./beerData');

console.clear();

console.log('beerData:');
console.table(beerData);

var uniqueTypes = R.pipe(R.pluck('type'), R.uniq);
var typeList = uniqueTypes(beerData);

console.log('typeList: ', typeList);

var makeTypeFilter = R.propEq('type');

var displayBeerByType = type => console.table(R.filter(makeTypeFilter(type), beerData));

typeList.forEach(displayBeerByType);